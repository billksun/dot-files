{ config, pkgs, lib, ... }:
{
  # Enable hardware features
  hardware = { 
    enableRedistributableFirmware = true;
    # pulseaudio.enable = true;
    bluetooth.enable = true;
  };

  # Use the extlinux boot loader. (NixOS wants to enable GRUB by default)
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;

  # if you have a Raspberry Pi 2 or 3, pick this:
  boot.kernelPackages = pkgs.linuxPackages_latest;
  # boot.kernelPackages = pkgs.linuxPackages_4_19;

  # A bunch of boot parameters needed for optimal runtime on RPi 3b+
  boot.kernelParams = ["cma=320M"];

  # Set time zone
  time.timeZone = "America/Los_Angeles";

  nixpkgs.config.allowUnfree = true;
  environment.systemPackages = with pkgs; [
    # libraspberrypi
    helix
    nssTools
  ];

  programs.bash.completion.enable = true;
  programs.firefox.enable = true;

  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  # Preserve space by sacrificing documentation and history
  documentation.nixos.enable = false;
  boot.tmp.cleanOnBoot = true;

  # Configure basic SSH access
  services.openssh = {
    enable = true;
  };

  # Enable the X11 windowing system
  # services.xserver.enable = true;
  # services.xserver.xkb.layout = "us";
  # services.xserver.videoDrivers = [ "modesetting" ];

  # Enable desktop environment
  # services.displayManager.sddm = {
  #   enable = true;
  #   wayland.enable = true;
  #   autoLogin.enable = true;
  #   autoLogin.user = "pi";
  # };

  # services.desktopManager.plasma6.enable = true;

  # Enable avahi
  # services.avahi = {
  #   enable = true;
  #   publish.enable = true;
  #   publish.userServices = true;
  # };

  # Enable print server
  # services.printing.enable = true;
  # services.printing.defaultShared = true;
  # services.printing.listenAddresses = [ "192.168.29.101:631" ];
  # services.printing.extraConf = ''
  #   # Allow remote administration...
  #   <Locatikioskon />
  #     Order allow,deny
  #     Allow @LOCAL
  #   </Location>
  #   # Allow remote administration...
  #   <Location /admin>
  #     Order allow,deny
  #     Allow @LOCAL
  #   </Location>
  # '';
  # services.printing.drivers = [
  #   pkgs.gutenprint
  #   pkgs.hplip
  #   pkgs.brlaser
  # ];

  services.cage = {
    enable = true;
    user = "pi";
    # program = "${pkgs.ungoogled-chromium}/bin/chromium --kiosk http://192.168.29.82";
    program = pkgs.writeShellScript "start-cage-app" ''
      ${pkgs.wlr-randr}/bin/wlr-randr --output HDMI-A-1
      exec ${pkgs.ungoogled-chromium}/bin/chromium --kiosk http://10.0.0.158
    '';
  };

  services.fwupd.enable = true;

  # Network configuration
  networking = {
    hostName = "sunpi";
    networkmanager.enable = true;
  };

  # Use 1GB of additional swap memory in order to not run out of memory
  # when installing lots of things while running other things at the same time.
  swapDevices = [ { device = "/swapfile"; size = 4096; } ];

  # Additional Nix configuration
  nix = {
    settings = {
      trusted-public-keys = [
        "apollo-1:R0Dn0Ch7loqBzrkqYR4qJsKOWdnZEcHLKVzTLG54Uzxh7qLEqityaJ01blzCX6uhxB7uX2w7tCKEKEdYCdzWDA=="
      ];
      trusted-users = [ "root" "bill" ];
    };
    gc.automatic = true;
    gc.options = "--delete-older-than 30d";
    extraOptions = ''
      auto-optimise-store = true
      keep-outputs = true
      experimental-features = nix-command flakes
    '';
  };

  # Users
  users.users = {
    root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMdm5bUm2kQbLhPLosgnlvqV9SZBOMRtIQ9H/U/nmB4S billksun@gmail.com"
      ];
    };
    pi = {
      isNormalUser = true;
    };
  };

  system.stateVersion = "24.11";
}
