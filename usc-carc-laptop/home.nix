{ pkgs, lib, vscode-utils, config, ... }:

with pkgs;

let
  vscodeExtensionsFromMarketplace = import ./vscode/extensions.nix;
in
{
  manual.manpages.enable = false;
  nixpkgs.config.allowUnfree = true;
  
  home.username = "bill";
  home.homeDirectory = "/home/bill";
  home.sessionPath = [ "~/.local/bin" ];
  home.sessionVariables = { 
    EDITOR = "hx";
  };
  home.stateVersion = "22.11";
  home.packages = with pkgs; [
    # Utilities

    lm_sensors
    wayland-utils
    vulkan-tools
    glxinfo
    ripgrep
    pass
    openconnect
    python3
    xorg.xmessage
    unzip
    # unar
    p7zip
    imagemagick
    tesseract
    qpdf
    python39Packages.weasyprint
    texlive.combined.scheme-full
    tectonic
    pandoc
    plantuml
    pandoc-plantuml-filter
    nuspell
    aspell
    aspellDicts.en
    hunspellDicts.en_US
    xdotool
    xclip
    restic
    gnome-network-displays
    virt-manager
    ark
    appimage-run

    # Applications

    chrysalis
    kcolorchooser
    ktimetracker
    # logseq
    anytype
    skanlite
    kdePackages.skanpage
    ktorrent
    digikam
    gwenview
    krita
    inkscape
    kate
    okular
    discord
    slack
    neochat
    signal-desktop
    # jami-daemon
    # jami-client-qt
    keepassxc
    google-chrome
    libreoffice-qt
    thunderbird
    kalendar
    kdePackages.kdepim-addons
    kdePackages.akonadiconsole
    kdePackages.krdc
    kdePackages.krfb
    kdePackages.filelight
    kdePackages.kasts
    vlc
    haruna
    elisa
    freetube
    # zotero
    hledger
    hledger-ui
    hledger-web
    # haskellPackages.hledger-flow
    pencil
    zoom-us
    dbeaver-bin
    exercism
    phoronix-test-suite
    moonlight-qt
    rustdesk

    # Games

    lutris
    protontricks
    openssl # Lutris' game installer requires this
    zenity # Lutris' game installer requires this
    heroic # Epic Games Launcher

    # Runtimes

    jre

    # Development tools
    
    nil
    kdiff3
    cachix
    nix-prefetch-git
    patchelf
    arduino
  ]; 
#   ++ builtins.filter pkgs.lib.isDerivation (builtins.attrValues plasma5Packages);

  programs.home-manager.enable = true;

  programs.niri.settings = {
    outputs."eDP-1".scale = 1.75;
    input.touchpad = {
      dwt = true;
      click-method = "clickfinger";
    };
    spawn-at-startup = [
      { command = [(lib.getExe' pkgs.cosmic-panel "cosmic-panel")]; }
      { command = [(lib.getExe' pkgs.cosmic-launcher "cosmic-launcher")]; }
    ];
    binds = with config.lib.niri.actions; {
      "Mod+Shift+E".action = quit;

      "Mod+T".action.spawn = "foot";
      "Mod+D".action.spawn = "cosmic-launcher";
      "Mod+Backspace".action = close-window;

      "Mod+Minus".action = set-column-width "-10%";
      "Mod+Equal".action = set-column-width "+10%";
      "Mod+Shift+Minus".action = set-window-height "-10%";
      "Mod+Shift+Equal".action = set-window-height "+10%";
    };
  };

  programs.foot = {
    enable = true;
    settings = {
      main = {
        font = "Monospace:size=9";
        dpi-aware = "yes";
      };

      mouse = {
        hide-when-typing = "yes";
      };

      cursor = {
        color = "ffffff 009a8a";
      };
      colors = {
        background = "ffffff";
        foreground = "474747";

        regular0 = "ebebeb";
        regular1 = "d6000c";
        regular2 = "1d9700";
        regular3 = "c49700";
        regular4 = "0064e4";
        regular5 = "dd0f9d";
        regular6 = "00ad9c";
        regular7 = "878787";

        bright0 = "cdcdcd";
        bright1 = "bf0000";
        bright2 = "008400";
        bright3 = "af8500";
        bright4 = "0054cf";
        bright5 = "c7008b";
        bright6 = "009a8a";
        bright7 = "282828";
      };
    };
  };
  
  programs.bat = {
    enable = true;
    config = {
      theme = "Solarized (light)";
    };
  };

  programs.starship = {
    enable = true;
    settings = {
      character = {
        success_symbol = "[λ](bold)";
        error_symbol = "[λ](bold red)";
      };
      package = {
        symbol = "";
      };
      time = {
        disabled = false;
      };
    };
  };

  programs.fish = {
    enable = true;
  };

  programs.bash = {
    enable = false;
    enableVteIntegration = true;
    initExtra = ''
      . "/etc/profiles/per-user/bill/etc/profile.d/hm-session-vars.sh"
    '';
  };
  
  programs.ssh = {
    enable = true;
    matchBlocks = {
      "disco.usc.edu" = {
        identityFile = "~/.ssh/id_ed25519_usc";
        identitiesOnly = true;
        user = "billsun";
      };
      "hpc-ood.usc.edu" = {
        identityFile = "~/.ssh/id_ed25519_usc";
        identitiesOnly = true;
        proxyJump = "disco.usc.edu";
      };
    };
  };
  
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
  
  programs.fzf = {
    enable = true;
  };

  programs.git = {
    enable = true;
    difftastic.enable = true;
    userName = "Bill Sun";
    userEmail = "billksun@gmail.com"; 
    includes = [
      { 
        path = "~/Projects/USC/git-config.inc";
        condition = "gitdir:~/Projects/USC/";
      }
    ];
    extraConfig = {
      merge.tool = "kdiff3";
      init = {
        defaultBranch = "main";
      };
      pull = { 
        rebase = true;
      };
      push = {
        autoSetupRemote = true;
      };
    };
  };

  programs.command-not-found = {
    enable = true;
  };

  programs.bottom = {
    enable = true;
    settings = {
      flags = {
        color = "default-light";
      };
    };
  };

  programs.broot = {
    enable = true;
  };

  # programs.firefox = {
  #   enable = true;
  # };

  programs.helix = {
    enable = true;
    settings = {
      theme = "everforest_light";
      editor = {
        whitespace.render = "all";
        lsp.display-messages = true;
        file-picker.hidden = false;
        true-color = true;
        color-modes = true;
        cursorline = true;
        soft-wrap.enable = true;
      };
      keys.normal = {
        C-e = "half_page_up";
      };
    };
    languages = {
      language-server = {
        haskell = {
          command = "haskell-language-server-wrapper";
          args = ["--lsp"];
        };
      };
      language = [
        {
          name = "haskell";
          language-servers = [
            { name = "haskell"; }
          ];
        }
        # {
        #   name = "tailwindcss";
        #   scope = "source.svelte";
        #   roots = ["package.json"];
        #   file-types = ["svelte" "hs"];
        #   language-server = {
        #     command = "npx";
        #     args = ["tailwindcss-language-server" "--stdio"];
        #  };
        # }
      ];
    };
  };
      
  programs.vscode = {
    enable = true;
    userSettings = {
      "workbench.colorTheme" = "Quiet Light";
      "files.autoSave" = "afterDelay";
      "window.zoomLevel" = 0;
      "window.titleBarStyle" = "native";
      "window.menuBarVisibility" = "toggle";
      "editor.fontLigatures" = true;
      "editor.fontFamily" = "'Monospace', 'Droid Sans Fallback'";
      "editor.formatOnType" = true;
      "editor.renderControlCharacters" = false;
      "git.autofetch" = true;
      "git.enableSmartCommit" = true;
      "http.proxySupport" = "override";
      "omnisharp.path" = "/home/bill/.nix-profile/bin/omnisharp";
      "omnisharp.useGlobalMono" = "never";
      "purescript.addSpagoSources" = true;
      "purescript.buildCommand" = "spago build -- --json-errors";
      "latex-workshop.view.pdf.viewer" = "tab";
      "markdown-preview-enhanced.latexEngine" = "tectonic";
      "cSpell.enabledLanguageIds" = [
        "asciidoc"
        "c"
        "cpp"
        "csharp"
        "css"
        "git-commit"
        "go"
        "handlebars"
        "haskell"
        "html"
        "jade"
        "java"
        "javascript"
        "javascriptreact"
        "json"
        "jsonc"
        "latex"
        "less"
        "markdown"
        "org"
        "php"
        "plaintext"
        "pug"
        "python"
        "restructuredtext"
        "rust"
        "scala"
        "scss"
        "text"
        "typescript"
        "typescriptreact"
        "yaml"
        "yml"
      ];
      "tailwindCSS.includeLanguages" = { 
        "haskell" = "html";
        "plaintext" = "html";
      };
      "angular.experimental-ivy" = true;
    };
    extensions = with pkgs.vscode-extensions; [
      ms-vsliveshare.vsliveshare
      ms-dotnettools.csharp
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace vscodeExtensionsFromMarketplace.extensions;
  };
      
  programs.obs-studio = {
    enable = true;
    plugins = with pkgs.obs-studio-plugins; [ 
      droidcam-obs
      obs-backgroundremoval
      obs-pipewire-audio-capture
      obs-gstreamer
      wlrobs
    ];
  };

  programs.mangohud = {
    enable = true;
  };
      
  # services.easyeffects = {
  #   enable = true;
  #   preset = "Noise Reduction";
  # };
      
  services.syncthing = {
    enable = true;
    tray = {
      enable = true;
    };
  };
      
  services.mpris-proxy.enable = true;
  
  # Systemd customizations for loading syncthingtray in Plasma 5

  systemd.user.targets.tray = {
		Unit = {
			Description = "Home Manager System Tray";
			Requires = [ "graphical-session-pre.target" ];
		};
	};

  systemd.user.services.${config.services.syncthing.tray.package.pname} = {
    Install.WantedBy = lib.mkForce [ ];
  };
  
  systemd.user.timers.${config.services.syncthing.tray.package.pname} = {
    Timer = {
      OnActiveSec = "10s";
      AccuracySec = "1s";
    };
    Install = { WantedBy = [ "graphical-session.target" ]; };
  };
}
