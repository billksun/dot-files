{
  description = "NixOS System Configuration";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-cosmic = {
      # url = "github:billksun/nixos-cosmic-niri";
      url = "github:lilyinstarlight/nixos-cosmic";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    cosmic-ext-alternative-startup.url = "github:bluelinden/cosmic-ext-alternative-startup";
    niri = {
      url = "github:sodiboo/niri-flake";
    };
    yazi-flavors = {
      url = "github:yazi-rs/flavors";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, home-manager, nixos-cosmic, niri, cosmic-ext-alternative-startup, yazi-flavors, ... }: {
    nixosConfigurations = {
      apollo = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {
            _module.args = { inherit niri nixos-cosmic cosmic-ext-alternative-startup yazi-flavors; };
            nix.settings = {
              substituters = [ "https://cosmic.cachix.org/" ];
              trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE" ];
            };
          }
          nixos-cosmic.nixosModules.default
          niri.nixosModules.niri
          ./apollo/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.bill = import ./apollo/home.nix;
            home-manager.extraSpecialArgs = {
              inherit cosmic-ext-alternative-startup;
            };
          }
        ];
      };

      usc-hp-elitebook-x360-1040-g8 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          {
            nix.settings = {
              substituters = [ "https://cosmic.cachix.org/" ];
              trusted-public-keys = [ "cosmic.cachix.org-1:Dya9IyXD4xdBehWjrkPv6rtxpmMdRel02smYzA85dPE" ];
            };
          }
          nixos-cosmic.nixosModules.default
          ./usc-carc-laptop/configuration.nix
          home-manager.nixosModules.home-manager {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.bill = import ./usc-carc-laptop/home.nix;
          }
        ];
      };

      sunpi = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = [
          ./sunpi/configuration.nix
        ];
      };

      ring = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./ring/configuration.nix
        ];
      };
    };
  };
}
