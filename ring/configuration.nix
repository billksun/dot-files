# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = 1;
  };

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users = {
    root = {
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMdm5bUm2kQbLhPLosgnlvqV9SZBOMRtIQ9H/U/nmB4S billksun@gmail.com"
      ];
    };
    bill = {
      isNormalUser = true;
      description = "Bill Sun";
      extraGroups = [ "networkmanager" "wheel" ];
      packages = with pkgs; [];
    };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #  wget
    helix
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.tailscale = {
    enable = true;
    authKeyFile = "/home/root/.config/tailscale/tailscale.key";
  };


  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Network configuration

  networking.usePredictableInterfaceNames = false;
  services = {
    udev.extraRules = ''
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:d8", NAME="wan"
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:d9", NAME="lan0"
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:da", NAME="lan1"
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:db", NAME="lan2"
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:dc", NAME="lan3"
      ACTION=="add", SUBSYSTEM=="net", ATTR{address}=="60:be:b4:11:5b:dd", NAME="lan4"
    '';
  };

  networking = {
    networkmanager.enable = lib.mkForce false;    
    useNetworkd = true;
    hostName = "ring";
    useDHCP = true;
    # defaultGateway = {
    #   address = "192.168.29.1";
    #   interface = "wan";
    # };
    # nameservers = ["192.168.29.2"];
    bridges = {
      br0 = {
        interfaces = [ "lan0" "lan1" "lan2" "lan3" "lan4" ];
      };
    };
    nat = {
      enable = true;
      externalInterface = "wan";
      internalInterfaces = [ "br0" ];
      internalIPs = [ "10.0.0.0/24" ]; 
    };
    interfaces = {
      wan = {
        useDHCP = true;
        tempAddress = "disabled";
      };
      br0 = {
        ipv4.addresses = [ { address = "10.0.0.1"; prefixLength = 24; } ];
        useDHCP = false;
        macAddress = "AA:BB:CC:DD:EE:FF";
      };
    };
    firewall.interfaces = {
      br0 = {
        allowedUDPPorts = [ 53 67 ];
        allowedTCPPorts = [ 53 67 ];
      };
      tailscale0 = {
        allowedUDPPorts = [ 53 ];
        allowedTCPPorts = [ 53 ];
      };
    };
  };

  services.resolved.enable = false;
  services.dnsmasq = {
    enable = true;
    alwaysKeepRunning = true;
    settings = {
      interface = [ "br0" "tailscale0" ];
      local = "/lan/";
      domain = "lan";
      server = [ "1.1.1.1" ];
      domain-needed = true;
      bogus-priv = true;
      no-resolv = true;
      no-hosts = true;
      cache-size = 1000;
      expand-hosts = true;

      address = "/ring.lan/10.0.0.1";
      
      dhcp-range = [ 
        "10.0.0.10,10.0.0.254,5m"
      ];
      dhcp-option = ["option:router, 10.0.0.1"];
      dhcp-host = [
        "14:dd:a9:87:74:28,asus-ap"
        "1c:64:99:ea:c3:6a,powerline-master"
        "1c:64:99:98:1c:dd,powerline-garage"
        "a0:18:42:5a:11:5c,powerline-livingroom"
        "a0:18:42:5a:11:5d,powerline-nursery"
        "c4:6e:1f:11:e5:db,solar-power"
        "a0:60:32:01:9d:18,cam-driveway"
        "a0:60:32:06:35:54,cam-frontdoor"
        "a0:60:32:01:a4:d2,cam-nursery"
      ];
    };
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

}
